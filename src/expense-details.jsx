import React from 'react'

const ExpenseDetails = ({ expense }) => (
	<div className="container">
		<div className='row'>
			<div className='col-md-5'>Category</div>
			<div className='col-md-5'>{expense.category}</div>
		</div>
		<div className='row'>
			<div className='col-md-5'>Date</div>
			<div className='col-md-5'>{expense.date}</div>
		</div>
		<div className='row'>
			<div className='col-md-5'>Amount</div>
			<div className='col-md-5'>{expense.amount.value}</div>
		</div>
		<div className='row'>
			<div className='col-md-5'>Description</div>
			<div className='col-md-5'>{expense.description}</div>
		</div>
		<div className='row'>
			<div className='col-md-5'>Status</div>
			<div className='col-md-5'>{expense.status ? 'DONE' : 'PENDING'}</div>
		</div>
	</div>
)

export default ExpenseDetails
