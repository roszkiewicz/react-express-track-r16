import uuid from 'uuid/v4'
import React from 'react'

class Form extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			category: '',
			date: '',
			amount: '',
			description: '',
		}
	}

	update = e => {
		const { value, name } = e.target
		this.setState({
			[name]: value,
		})
	}

	add = () => {
		const { category, date, amount, description } = this.state
		const id = uuid()

		this.props.onAdd({
			id,
			category,
			date,
			description,
			amount: {
				value: parseFloat(amount, 10),
				currency: '$'
			},
			status: false,
		})

	}

	render () {
		const { category, date, amount, description } = this.state
		return (
			<div>
				<div className='row'>
					<div className='col-md-4'>Category</div>
					<div className='col-md-8'>
						<input name='category' value={category} onChange={this.update}/>
					</div>
				</div>
				<div className='row'>
					<div className='col-md-4'>Date</div>
					<div className='col-md-8'>
						<input name='date' value={date} onChange={this.update}/>
					</div>
				</div>
				<div className='row'>
					<div className='col-md-4'>Amount</div>
					<div className='col-md-8'>
						<input name='amount' value={amount} onChange={this.update}/>
					</div>
				</div>
				<div className='row'>
					<div className='col-md-4'>Description</div>
					<div className='col-md-8'>
						<input name='description' value={description} onChange={this.update}/>
					</div>
				</div>
				<div>
					<button onClick={this.add}>Dodaj Panie</button>
				</div>
			</div>
		)
	}
}

export default Form